package com.dvereykin.program6;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.*;

/**
 * Created by Dmitry Vereykin aka eXrump on 11/19/2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String TABLE_ADDRESS = "address";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_STATE = "state";
    public static final String COLUMN_ZIPCODE = "zipcode";
    public static final String COLUMN_IMAGE = "image";

    private static final String DATABASE_NAME = "PersonalDataWithPhotos.db";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE_SQL = "create table "
            + TABLE_ADDRESS  + "("
            + COLUMN_ID      + " integer primary key autoincrement, "
            + COLUMN_NAME    + " text not null, "
            + COLUMN_ADDRESS + " text not null, "
            + COLUMN_CITY + " text not null, "
            + COLUMN_STATE + " text not null, "
            + COLUMN_ZIPCODE + " text not null, "
            + COLUMN_IMAGE + " text not null);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
        onCreate(db);
    }
}

