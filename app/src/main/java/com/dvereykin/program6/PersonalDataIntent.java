package com.dvereykin.program6;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Dmitry Vereykin aka eXrump on 9/29/2016.
 */


class PersonalDataIntent {
    public String name;
    public String address;
    public String city;
    public String state;
    public String zipCode;
    public String image;

    public enum ActionType {
        ADD,
        EDIT,
        DELETE,
        VIEW
    }

    ActionType action;
    int addressIndex = 0;
    Intent intent;

    public PersonalDataIntent(Intent intent) {
        Bundle bundle = intent.getExtras();
        try {
            name = bundle.getString("name");
            address = bundle.getString("address");
            city = bundle.getString("city");
            state = bundle.getString("state");
            zipCode = bundle.getString("zipCode");
            action = ActionType.values()[bundle.getInt("action",0)];
            addressIndex = bundle.getInt("addressIndex");
            image = bundle.getString("image");
        } catch (Exception ex) {
            ex.getMessage();
        }
    }

    public PersonalDataIntent() {
        name = "";
        address = "";
        city = "";
        state = "";
        zipCode = "";
        image = "";
    }

    public PersonalDataIntent(PDAttributeGroup addressAttributes, ActionType action, int addressIndex) {
        name = addressAttributes.name;
        address = addressAttributes.address;
        city = addressAttributes.city;
        state = addressAttributes.state;
        zipCode = addressAttributes.zipCode;
        image = addressAttributes.image;
        this.action = action;
        this.addressIndex = addressIndex;
    }


    public void clearIntent() {
        intent = null;

    }

    void putExtras() {
        intent.putExtra("name", name);
        intent.putExtra("address", address);
        intent.putExtra("city", city);
        intent.putExtra("state", state);
        intent.putExtra("zipCode", zipCode);
        intent.putExtra("action", action.ordinal());
        intent.putExtra("addressIndex", addressIndex);
        intent.putExtra("image", image);
    }

    public Intent getIntent() {
        if (intent == null) {
            intent = new Intent();
            putExtras();
        }
        return intent;
    }

    public Intent getIntent(Activity addressEntry,
                            Class<EditValuesActivity> class1) {
        if (intent == null) {
            intent = new Intent(addressEntry, class1);
            putExtras();
        }
        return intent;
    }

}
